# miscellaneous
Miscellaneous small projects! 

# Now includes Penguin Cam! 
Meant for Linux based display drivers and firefox. Be sure to configure an auto login kiosk feature; place the start.sh script in the directory: "/home/kiosk/.config/autostart/start.sh", and the html page within the home directory of whatever that account is. To kick everything off you can either reboot the display driver or ssh in and switch users to the kisok account to run your awesome penguin cam. Does it work? Sure! I have tested it on an older RHEL-6 display driver so it should probably work on other distros.

# KIOSK TODO
Did you see the html? Pretty gross. Ideally I should be caching the images locally with wget, or doing something with python. This is just a down and dirty way to demo your cool display drivers kiosk feature. 


