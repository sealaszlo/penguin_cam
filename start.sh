#!/bin/bash
export DISPLAY=":0.0"

# Disable screensaver and blanking. A sleep statement
# may be necessary in some cases before the `xset` commands
xset s 0 0
xset s off -dpms

# Kill all running firefox processes
killall -9 firefox

# Nab defunct processes and kill them too
parents_of_dead_kids=$(ps -ef | grep [d]efunct | awk '{print $3}' | sort | uniq | egrep -v '^1$');
echo "$parents_of_dead_kids" | xargs kill

# Launch FF with weather
firefox ~/antaractic.html &
sleep 5 

# Find FF and smash F11 to make it fullscreen and move the cursor out of view
xdotool search --sync --onlyvisible --class "Firefox" windowactivate key F11
xdotool mousemove 800 1080
